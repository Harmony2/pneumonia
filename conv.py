from keras import models
from keras import layers
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
import matplotlib.pyplot as plt

train_datagen = ImageDataGenerator(rescale=1./255, horizontal_flip=True, zoom_range=0.1)
val_datagen = ImageDataGenerator(rescale=1./255)

train_gen = train_datagen.flow_from_directory("D:\\scripts\\pneumonia\\chest_xray\\train\\", target_size=(250,250), batch_size=20, class_mode="binary")
val_gen = train_datagen.flow_from_directory("D:\\scripts\\pneumonia\\chest_xray\\val\\", target_size=(250,250), batch_size=20, class_mode="binary")

model = models.Sequential()

model.add(layers.Conv2D(32, (3,3), activation="relu", input_shape=(250,250,3)))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Conv2D(64, (3,3), activation="relu"))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Conv2D(128, (3,3), activation="relu"))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Flatten())
model.add(layers.Dense(1000, activation="relu"))
model.add(layers.Dense(1, activation="sigmoid"))

model.compile(loss="binary_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

history = model.fit_generator(train_gen, steps_per_epoch=521, epochs=30, validation_data=val_gen, validation_steps=50)

model.save("pneumonia_2.h5")

acc = history.history["acc"]
val_acc = history.history["val_acc"]
epochs = range(1, len(acc)+1)

plt.plot(epochs, acc, "blue", label="Train")
plt.plot(epochs, val_acc, "green", label="Validate")
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.show()

#5218 = total train samples
#10436 = double